import { createAppContainer, createStackNavigator } from 'react-navigation';
import DetailsScreen from './views/DetailsScreen';
import HomeScreen from './views/HomeScreen';

const AppNavigator = createStackNavigator(
    {
        Home: {
            screen: HomeScreen,
        },
        Details: {
            screen: DetailsScreen,
        },
    },
    {
        initialRouteName: 'Home',
    },
);

export default createAppContainer(AppNavigator);
