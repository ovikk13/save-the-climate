import * as React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import * as Navigation from 'react-navigation';

export default (props: Navigation.ScreenProps) => (
    <View style={styles.home}>
        <Text>Home Screen</Text>
        <Button
            title="Go to details"
            onPress={() => props.navigation.navigate('Details')}
        />
    </View>
);

const styles = StyleSheet.create({
    home: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
