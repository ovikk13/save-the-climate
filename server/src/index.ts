import dotenv from "dotenv";
import express from "express";

const ParseServer = require('parse-server').ParseServer;

dotenv.config();

const app = express();
const port = process.env.SERVER_PORT;

const api = new ParseServer({
    databaseURI: 'mongodb://localhost:27017/test',
    appId: 'test',
    masterKey: 'blablabla'
});

app.use('/parse', api);

app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log( `server started at http://localhost:${ port }` );
});
